import firebase from "config/fbConfig.js"
import axios from "axios";

export const startTracking = (billNum) => {
  return (dispatch) => {
    let acc = {}
    const collectionString = "TESTbills"

    // fetch delivery info from firebase
    const db = firebase.firestore()

    db.collection(collectionString).get().then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
      });
    });

    var docRef = db.collection(collectionString).doc(billNum)

    docRef.get().then((doc) => {
      if (doc.exists) {
        const data = doc.data()

        // Format Firebase Delivery Info for mapping    
        acc = {
          destination: {
            lat: data.destination.latitude,
            lng: data.destination.longitude
          },
          origin: {
            lat: data.origin.latitude,
            lng: data.origin.longitude
          },
          truckNum: data.truckNum
        }
      } else {
        // doc.data() will be undefined in this case
        dispatch(mapError())
      }
    }).then(() => {
      // If there is a truck number get the truck location
      if (!!acc.truckNum === true) {
        // request keep trucking for delivery info       
        let url = `https://galvanize-cors.herokuapp.com/https://api.keeptruckin.com/v1/vehicle_locations?vehicle_ids=${acc.truckNum}`
        axios.get(
          url,
          {
            "headers": {
              "x-api-key": process.env.KEEPTRUCKING_API_KEY
            }
          }
        ).then((response) => {
          // add truck location to accumulator
          const location = response.data.vehicles[0].vehicle.current_location
          acc = {
            ...acc,
            truckLocation: {
              lat: location.lat,
              lng: location.lon
            }
          }
          dispatch(tracking(acc))
        })
          .catch((error) => {
            console.log(error)
          })
      } else {
        dispatch(tracking(acc))
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
  }
}

export const tracking = (deliveryInfo = {}) => ({
  type: "TRACK",
  deliveryInfo
})