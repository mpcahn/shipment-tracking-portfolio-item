/* global google */

import React from 'react';
import { connect } from 'react-redux';
import { compose, withProps, lifecycle } from 'recompose';
import { addMapData } from '../../store/actions/assetTrackingActions'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker
} from 'react-google-maps';
import {
  darkMapStyles as mapStyles
} from "./MapsStyles";

class WaypointsMap extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    // Determine if using east or west truck icon.
    let truckIcon
    this.props.trackingLocationData.origin.lng - this.props.trackingLocationData.destination.lng > 0 ?
      truckIcon = "/truck-icon-west.png"
      :
      truckIcon = "/truck-icon-east.png"

    const truckExists = !!this.props.trackingLocationData.truckLocation
    let markerLocation
    let waypoints
    let mapProps1
    if (truckExists === true) {
      // if there is a truck location
      // Add truck location as first waypoint    
      markerLocation = this.props.trackingLocationData.truckLocation
      waypoints = [
        {
          location: markerLocation,
          stopover: true
        }
      ]
      mapProps1 = {
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key="+process.env.GOOGLE_MAPS_API_KEY,
        loadingElement: <div style={{ height: `350px` }} />,
        containerElement: <div style={{ width: `100%` }} />,
        mapElement: <div style={{ height: `350px`, width: `auto` }} />,
        origin: this.props.trackingLocationData.origin,
        waypoints,
        destination: this.props.trackingLocationData.destination
      }
    } else {
      // If there is no truck put the marker at origin and have there be no waypoints
      markerLocation = this.props.trackingLocationData.origin
      mapProps1 = {
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key="+process.env.GOOGLE_MAPS_API_KEY,
        loadingElement: <div style={{ height: `350px` }} />,
        containerElement: <div style={{ width: `100%` }} />,
        mapElement: <div style={{ height: `350px`, width: `auto` }} />,
        origin: this.props.trackingLocationData.origin,
        destination: this.props.trackingLocationData.destination
      }
    }

    const mapDispatchToProps = (dispatch) => ({
      addMapData: (data) => dispatch(addMapData(data))
    })

    const DirectionsComponent = compose(
      withProps(mapProps1),
      withScriptjs,
      withGoogleMap,
      connect(null, mapDispatchToProps),
      lifecycle({
        componentDidMount() {
          const DirectionsService = new google.maps.DirectionsService();
          let mapProps2 = {
            origin: this.props.origin,
            destination: this.props.destination,
            travelMode: 'DRIVING'
          };
          // add waypoints if there is a truck location
          truckExists ? (
            mapProps2 = {
              ...mapProps2,
              waypoints: this.props.waypoints,
              optimizeWaypoints: true,
            }) : (null);
          DirectionsService.route(mapProps2, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
              this.setState({
                directions: { ...result },
                markers: true
              })
              // add all return data to store including parsed distance from truck
              let legsArr = result.routes[0].legs
              const distance = legsArr.reduce((acc, leg) => acc + leg.distance.value, 1)
              this.props.addMapData({ ...result, distance })

            } else {
              console.error(`error fetching directions ${result}`);
            }
          });
        }
      })
    )(props =>
      <GoogleMap
        defaultZoom={3}
        defaultOptions={{
          styles: mapStyles
        }}
      ><Marker
          position={markerLocation}
          icon={{
            url: truckIcon,
            scaledSize: new google.maps.Size(40, 20)
          }}
        />
        {
          props.directions &&
          <DirectionsRenderer
            directions={props.directions}
            options={{
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: "#7ddb46",
                strokeOpacity: 0.5,
                strokeWeight: 4
              }
            }}
          />}
      </GoogleMap>
    );
    return (
      <DirectionsComponent />
    )
  }
}

const mapStateToProps = (state) => ({
  trackingLocationData: state.trackingLocationData
})

export default connect(mapStateToProps)(WaypointsMap)
