import React from "react";
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import LinearProgress from '@material-ui/core/LinearProgress';

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";

import DirectionsMap from "components/Map/DirectionsMap"
import { startTracking } from "store/actions/assetTrackingActions"

import styles from "assets/jss/material-kit-pro-react/components/forms/trackingFormStyle.jsx";

class Form extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      billNum: '',
      billNumDisp: '',
      billError: false
    };
  }

  handleChange = (e) => {
    let str = e.target.value.replace(/\s/g, ''); // remove spaces
    console.log(str)
    this.setState({
      [e.target.id]: str
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    !!this.state.billNum ? (
      // this.props.trackingMapData.distance = null,
      // this.props.trackingLocationData.truckNum = null,
      this.setState({ billNumDisp: this.state.billNum }),
      this.props.startTracking(this.state.billNum)
    ) : (
        this.setState({
          billError: true
        })
      )
  }

  getMiles = (i) => {
    i = i * 0.000621371192
    return i.toFixed(2) + " miles"
  }

  render() {
    const { classes } = this.props;

    // progress used for progress bar and map rendering
    let progress
    if (this.props.trackingMapData.distance) {
      // If there's a distance and a truck number the order is in transit
      // set progress bar to show how far along the route the truck has gone 
      // Limited to the top 40% of the progress bar 
      if (this.props.trackingLocationData.truckNum) {
        let legs = this.props.trackingMapData.routes[0].legs
        let distanceTraveled = legs[0].distance.value
        let distanceToGo = legs[1].distance.value
        progress = 60 + Math.ceil((distanceTraveled / (distanceTraveled + distanceToGo)) * 40)

        // If there's a distance but no truck number it means the order
        // has been processed
      } else {
        progress = 32
      }
    }

    return (
      <GridContainer justify="center">
        <GridItem xs={12}>
          <br /> <br />
          <form
            style={{
              textAlign: "center"
            }}
            onSubmit={this.handleSubmit}
          >
            <GridContainer justify="center">
              <GridItem xs={11} sm={9}>
                <CustomInput
                  labelText="Bill Number"
                  id="billNum"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    type: "text",
                    onChange: (event) => this.handleChange(event)
                  }}
                />
              </GridItem>
              <GridItem xs={12} sm={3}>
                <Button
                  className={classes.ltrButton}
                  type="submit"
                  rel="noopener noreferrer"
                >
                  SUBMIT
              </Button>
              </GridItem>
            </GridContainer>
          </form>
          <GridContainer justify="center">
            <GridItem xs={11} sm={9}>
              {
                this.state.billError || !!this.props.trackingMapData.mapError ? (
                  <p className={classes.errorText}>Bill Number not recognized. <br /> Please enter a valid Bill Number or call customer service at (720) 277-0030</p>
                ) : null
              }
              {!this.state.billNumDisp ? (
                <h5 className={classes.bodyText1}> Enter your Bill Number above to track your shipment.You can find your Bill Number in your
                   email receipt.You can also find your Bill Number in your order history when you log into your account.</h5>                
              ) : null}
            </GridItem>
          </GridContainer>
          {
            // Render info box and map after tracking location data set in redux
            !!this.props.trackingLocationData.origin && this.state.billNumDisp && <GridContainer>
              <GridItem xs={11} sm={4}>
                <h3 className={classes.header1}>SHIPMENT<br />INFORMATION</h3>
                {
                  // Render if the keep trucking query is successful
                  progress > 59 ? (
                    <div>
                      <p className={classes.bodyText1}>Bill Number: {this.state.billNumDisp}</p>
                      <p className={classes.bodyText1}>Distance: {this.getMiles(this.props.trackingMapData.distance)}</p>
                      <p className={classes.bodyText1}>Start Location: {this.props.trackingMapData.routes[0].legs[0].start_address}</p>
                      <p className={classes.bodyText1}>Distance Traveled: {this.getMiles(this.props.trackingMapData.routes[0].legs[0].distance.value)}</p>
                      <p className={classes.bodyText1}>Truck Location: {this.props.trackingMapData.routes[0].legs[1].start_address}</p>
                      <p className={classes.bodyText1}>Distance to Destination: {this.getMiles(this.props.trackingMapData.routes[0].legs[1].distance.value)}</p>
                      <p className={classes.bodyText1}>Destination: {this.props.trackingMapData.routes[0].legs[1].end_address}</p>
                    </div>
                  ) : (null)
                }
                {
                  // Render if map renders but keep trucking fails i.e. no truck to track
                  progress === 32 ? (
                    <div>
                      <p className={classes.textGreen}>Your order is being processed and has not yet been shipped</p>
                      <p className={classes.bodyText1}>Bill Number: {this.state.billNumDisp}</p>
                      <p className={classes.bodyText1}>Distance: {this.getMiles(this.props.trackingMapData.distance)}</p>
                      <p className={classes.bodyText1}>Start Location: {this.props.trackingMapData.routes[0].legs[0].start_address}</p>
                      <p className={classes.bodyText1}>Destination: {this.props.trackingMapData.routes[0].legs[0].end_address}</p>
                    </div>
                  ) : (null)
                }
              </GridItem>
              <GridItem xs={12} sm={8}>
                <DirectionsMap />
                <br />
              </GridItem>
              <GridItem xs={12}>
                <GridContainer>
                  <GridItem xs={12}>
                    {/* PROGRESS BAR */}
                    <LinearProgress
                      variant="determinate"
                      value={progress}
                      classes={{
                        barColorPrimary: classes.linearBarColorPrimary
                      }}
                    />
                    <GridContainer>
                      <GridItem xs={3}>
                        <p
                          className={classes.progressText}
                          style={{
                            textAlign: "left"
                          }}
                        >Order Recieved</p>
                      </GridItem>
                      <GridItem xs={3}>
                        {progress === 32 ? (
                          <h4 className={classes.textGreen}
                          >Order Processed</h4>
                        ) : (
                            <p className={classes.progressText}
                              style={{
                                textAlign: "left"
                              }}
                            >Order Processed</p>
                          )}
                      </GridItem>
                      <GridItem xs={3}>
                        {progress > 59 ? (
                          <h4
                            className={classes.textGreen}
                            style={{
                              textAlign: "center"
                            }}
                          >In Transit</h4>
                        ) : (
                            <p className={classes.progressText}>In Transit</p>
                          )}

                      </GridItem>
                      <GridItem xs={3}>
                        <p
                          className={classes.progressText}
                          style={{
                            textAlign: "right"
                          }}
                        >Arrived at Destination</p>
                      </GridItem>
                    </GridContainer>
                  </GridItem>
                </GridContainer>
              </GridItem>
            </GridContainer>
          }
        </GridItem>
      </GridContainer>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapDispatchToProps = (dispatch) => ({
  startTracking: (billNum) => dispatch(startTracking(billNum))
})

const mapStateToProps = (state) => {
  return {
    trackingLocationData: state.trackingLocationData,
    trackingMapData: state.trackingMapData
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Form);
